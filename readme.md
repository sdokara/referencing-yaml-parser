# Referencing YAML Parser

A YAML parser using SnakeYAML, but with a trick:
You can load files with multiple documents that have references to each other (foreign keys, if you like).


## Usage

Say you have these classes:
```java
class Department { 
    String name;
    String address;
    Set<Employee> employees;
}

class Employee {
    String name;
    Department department;
    Set<Skill> skills;
}

class Skill { 
    String name;
}
```

Then, you could load something like this:
```yaml
!Department
name: IT
address: Somewhere 1
$employees:
  - John
---

!Department
name: Finances
address: Somewhere 2
$employees:
  - Jane
---

!Employee
name: John
$department: IT            # creates a reference cycle Department <-> Employee
skills:
  - name: Computers        # defines a nested Skill
$skills:
  - Speaking               # refers to a Skill defined with Jane
  - Writing                # refers to a Skill defined in the root
---

!Employee
name: Jane
$department: Finances
skills:
  - name: Speaking
$skills:
  - Computers
---

!Skill
name: Writing
---
```

using this code:
```java
class Main {
    public static void main(String[] args) {
        InputStream istream = Starter.class.getResourceAsStream("/data.yml");
        ReferencingYamlParser parser = new ReferencingYamlParser("name");
        parser.registerClasses(Department.class, Employee.class, Skill.class);
        List<Object> objects = parser.parse(istream);
    } 
}
```

The parser would traverse all objects and magically set them according to the references via names.

It does not matter where the objects were defined -- as a root object or as nested within another 
object. They can be referenced from anywhere. The only limitations are:

* there cannot be two objects with the same name and of the same type;
* one object cannot define both a nested object and a reference to another object;
* for collections, defining both nested and referenced objects is supported, they will be joined.

Two-way joining objects is also supported, just take care not to cause some `StackOverflowError`s
(e.g. in the `Object::toString`, `Object::equals`, and `Object::hashCode` methods).