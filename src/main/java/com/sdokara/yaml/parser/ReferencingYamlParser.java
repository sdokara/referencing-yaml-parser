package com.sdokara.yaml.parser;

import com.sdokara.yaml.parser.exceptions.*;
import javassist.*;
import lombok.EqualsAndHashCode;
import org.yaml.snakeyaml.TypeDescription;
import org.yaml.snakeyaml.Yaml;
import org.yaml.snakeyaml.introspector.PropertySubstitute;

import java.io.InputStream;
import java.lang.reflect.Modifier;
import java.lang.reflect.*;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import static com.sdokara.yaml.parser.ReflectionUtils.*;

public class ReferencingYamlParser {
    private static final String CLASS_SUFFIX = "$Yaml";

    private static final ClassPool pool = ClassPool.getDefault();

    private final Map<Class, Class> proxyMap = new ConcurrentHashMap<>();
    private final Map<Class, Class> reverseProxyMap = new ConcurrentHashMap<>();
    private final Map<Class, Set<Reference>> referenceMap = new ConcurrentHashMap<>();
    private final Set<TypeDescription> typeDescriptions = Collections.synchronizedSet(new HashSet<>());

    private String refName;

    public ReferencingYamlParser(String refName) {
        this.refName = refName;
    }

    public static void appendClassLoader(ClassLoader classLoader) {
        pool.appendClassPath(new LoaderClassPath(classLoader));
    }

    public void registerClasses(Class... classes) {
        resolveReferences(classes);
        for (Class clazz : classes) {
            proxyMap.computeIfAbsent(clazz, this::proxy);
        }
        for (Class clazz : classes) {
            Class proxyClass = proxyMap.get(clazz);
            addTypeDescriptor(clazz, proxyClass);
        }
    }

    public List<Object> parse(InputStream istream) {
        Yaml yaml = new Yaml();
        for (TypeDescription typeDescription : typeDescriptions) {
            yaml.addTypeDescription(typeDescription);
        }

        Iterator<Object> iterator = yaml.loadAll(istream).iterator();
        List<Object> objects = new ArrayList<>();
        while (iterator.hasNext()) {
            Object object = iterator.next();
            if (object != null) {
                objects.add(object);
            }
        }

        List<Object> innerObjects = new ArrayList<>();
        for (Object object : objects) {
            extractInnerObjects(object, innerObjects);
        }
        objects.addAll(innerObjects);

        Map<Class, Map<String, Object>> classRefObjectMap = new HashMap<>();
        for (Object object : objects) {
            classRefObjectMap.compute(object.getClass(), (clazz, refObjectMap) -> {
                try {
                    if (refObjectMap == null) {
                        refObjectMap = new HashMap<>();
                    }
                    String ref = get(object, String.class, refName);
                    if (refObjectMap.containsKey(ref)) {
                        throw new DuplicateReferenceFoundException(reverseProxyMap.get(object.getClass()), ref);
                    }
                    refObjectMap.put(ref, object);
                } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
                    throw new ReferenceResolutionException(e);
                }
                return refObjectMap;
            });
        }

        for (Object object : objects) {
            Class clazz = reverseProxyMap.get(object.getClass());
            Set<Reference> references = referenceMap.get(clazz);
            if (!references.isEmpty()) {
                for (Reference reference : references) {
                    reference.apply(object, classRefObjectMap);
                }
            }
        }

        return objects.stream()
                .map(o -> o instanceof Proxy ? ((Proxy)o).unwrap() : o)
                .collect(Collectors.toList());
    }


    @SuppressWarnings("SuspiciousMethodCalls")
    private void resolveReferences(Class... additionalClasses) {
        Set<Class> classes = new HashSet<>(proxyMap.keySet());
        Collections.addAll(classes, additionalClasses);
        for (Class clazz : classes) {
            referenceMap.computeIfAbsent(clazz, c -> {
                Set<Reference> references = new HashSet<>();
                for (Field field : c.getDeclaredFields()) {
                    if (!isBeanField(c, field)) {
                        continue;
                    }
                    Class<?> fieldType = field.getType();
                    if (classes.contains(fieldType)) {
                        references.add(new Reference(field));
                    } else if (Collection.class.isAssignableFrom(fieldType)) {
                        if (fieldType != Set.class && fieldType != List.class) {
                            throw new IllegalArgumentException("Type " + fieldType +
                                    " not supported as collection reference");
                        }
                        Type genericType = field.getGenericType();
                        Type typeArgument = ((ParameterizedType)genericType).getActualTypeArguments()[0];
                        if (classes.contains(typeArgument)) {
                            references.add(new CollectionReference(field, (Class)typeArgument));
                        }
                    }
                }
                return references;
            });
        }
    }

    private void addTypeDescriptor(Class<?> clazz, Class<?> proxyClass) {
        TypeDescription typeDescription = new TypeDescription(proxyClass, "!" + clazz.getSimpleName());
        for (Reference reference : referenceMap.get(clazz)) {
            String fieldName = reference.field.getName();
            if (reference instanceof CollectionReference) {
                CollectionReference collectionReference = (CollectionReference)reference;
                typeDescription.addPropertyParameters(fieldName,
                        proxyMap.get(collectionReference.genericType));
            } else {
                typeDescription.substituteProperty(new PropertySubstitute(fieldName,
                        proxyMap.get(reference.field.getType())));
            }
        }
        typeDescriptions.add(typeDescription);
    }

    private Class proxy(Class clazz) {
        Set<Reference> references = referenceMap.get(clazz);
        if (references.isEmpty()) {
            reverseProxyMap.put(clazz, clazz);
            return clazz;
        }

        try {
            CtClass ctClass = pool.get(clazz.getName());
            CtClass proxyCtClass = buildWrappingProxy(clazz, ctClass);
            for (Reference reference : references) {
                CtField ctField = reference.makeField(proxyCtClass);
                proxyCtClass.addField(ctField);
                proxyCtClass.addMethod(reference.makeGetter(ctField));
                proxyCtClass.addMethod(reference.makeSetter(ctField));
            }
            Class<?> proxyClass = proxyCtClass.toClass();
            reverseProxyMap.put(proxyClass, clazz);
            return proxyClass;
        } catch (NotFoundException | CannotCompileException e) {
            throw new ProxyingException(clazz, e);
        }
    }

    private static CtClass buildWrappingProxy(Class clazz, CtClass ctClass)
            throws CannotCompileException, NotFoundException {
        CtClass proxyCtClass = pool.makeClass(clazz.getName() + CLASS_SUFFIX);
        proxyCtClass.setSuperclass(ctClass);
        proxyCtClass.addInterface(pool.get(Proxy.class.getName()));
        proxyCtClass.addField(CtField.make("private static final Class clazz = " + clazz.getCanonicalName() + ".class;",
                proxyCtClass));
        proxyCtClass.addField(CtField.make("private final Object t;", proxyCtClass));
        proxyCtClass.addConstructor(CtNewConstructor.make(null, null, "{ this.t = clazz.newInstance(); }", proxyCtClass));
        proxyCtClass.addMethod(CtMethod.make("public final Object unwrap() { return t; }", proxyCtClass));

        for (CtMethod ctMethod : ctClass.getDeclaredMethods()) {
            int modifiers = ctMethod.getModifiers();
            if (Modifier.isStatic(modifiers) || !Modifier.isPublic(modifiers)) {
                continue;
            }
            CtMethod wrappedMethod = CtNewMethod.make(
                    String.format(
                            "public Object $%s(Object[] args) {\n" +
                                    "java.lang.reflect.Method method = t.getClass().getDeclaredMethod(\"%s\"%s);\n" +
                                    "Object result = method.invoke(t, args);\n" +
                                    "return result;\n" +
                                    "}",
                            ctMethod.getName(),
                            ctMethod.getName(),
                            ctMethod.getParameterTypes().length > 0 ?
                                    ", new Class[]{" + Arrays.stream(ctMethod.getParameterTypes())
                                            .map(ctClass1 -> ctClass1.getName() + ".class")
                                            .collect(Collectors.joining(", ")) + "}" :
                                    ", new Class[0]"),
                    proxyCtClass);
            proxyCtClass.addMethod(CtNewMethod.wrapped(
                    ctMethod.getReturnType(), ctMethod.getName(), ctMethod.getParameterTypes(),
                    ctMethod.getExceptionTypes(), wrappedMethod, null, proxyCtClass
            ));
        }
        return proxyCtClass;
    }

    public interface Proxy {
        Object unwrap();
    }

    private void extractInnerObjects(Object object, List<Object> innerObjects) {
        Set<Reference> references = referenceMap.getOrDefault(reverseProxyMap.get(object.getClass()),
                Collections.emptySet());
        for (Reference reference : references) {
            try {
                Field field = object.getClass().getSuperclass().getDeclaredField(reference.field.getName());
                field.setAccessible(true);
                if (reference instanceof CollectionReference) {
                    Collection collection = (Collection)field.get(object);
                    if (collection != null) {
                        Set referenceKeys = get(object, Set.class, reference.refFieldName);
                        if (referenceKeys == null) {
                            referenceKeys = new HashSet();
                            set(object, reference.refFieldName, Set.class, referenceKeys);
                        }
                        for (Object innerObject : collection) {
                            String ref = get(innerObject, String.class, refName);
                            if (referenceKeys.contains(ref)) {
                                throw new RuntimeException("Ref " + ref + " found overriding existing reference");
                            }
                            //noinspection unchecked
                            referenceKeys.add(ref);
                            innerObjects.add(innerObject);
                            extractInnerObjects(innerObject, innerObjects);
                        }
                    }
                } else {
                    Object innerObject = field.get(object);
                    if (innerObject != null) {
                        String referenceKey = get(object, String.class, reference.refFieldName);
                        if (referenceKey != null) {
                            throw new ReferenceResolutionException("Ref " + referenceKey + " found overriding existing reference");
                        }
                        String ref = get(innerObject, String.class, refName);
                        set(object, reference.refFieldName, String.class, ref);
                        innerObjects.add(innerObject);
                        extractInnerObjects(innerObject, innerObjects);
                    }
                }
            } catch (IllegalAccessException | NoSuchFieldException | NoSuchMethodException | InvocationTargetException e) {
                throw new ReferenceResolutionException(e);
            }
        }
    }


    @EqualsAndHashCode
    private class Reference {
        protected Field field;
        protected String refFieldName;

        Reference(Field field) {
            this.field = field;
            this.refFieldName = "$" + field.getName();
        }

        CtField makeField(CtClass proxyCtClass) throws NotFoundException, CannotCompileException {
            return makeField(proxyCtClass, String.class);
        }

        CtMethod makeGetter(CtField ctField) throws CannotCompileException {
            return CtNewMethod.getter(getterName(field.getType(), refFieldName), ctField);
        }

        CtMethod makeSetter(CtField ctField) throws CannotCompileException {
            return CtNewMethod.setter(setterName(refFieldName), ctField);
        }

        public void apply(Object object, Map<Class, Map<String, Object>> classRefObjectMap) {
            try {
                String referenceKey = get(object, String.class, refFieldName);
                if (referenceKey == null) {
                    return;
                }

                Class<?> fieldType = field.getType();
                Map<String, Object> map = classRefObjectMap.get(proxyMap.get(fieldType));
                if (map == null) {
                    throw new ReferenceValueResolutionException("No objects of type " + fieldType.getSimpleName());
                }
                Object referenceValue = map.get(referenceKey);
                if (referenceValue == null) {
                    throw new ReferenceNotFoundException(fieldType, referenceKey);
                }

                if (referenceValue instanceof Proxy) {
                    referenceValue = ((Proxy)referenceValue).unwrap();
                }
                set(object, field.getName(), fieldType, referenceValue);
            } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
                throw new ReferenceValueResolutionException(e);
            }
        }


        CtField makeField(CtClass proxyCtClass, Class type)
                throws NotFoundException, CannotCompileException {
            return new CtField(pool.get(type.getName()), refFieldName, proxyCtClass);
        }
    }

    @EqualsAndHashCode(callSuper = true)
    public class CollectionReference extends Reference {
        private Class genericType;

        CollectionReference(Field field, Class genericType) {
            super(field);
            this.genericType = genericType;
        }

        @Override
        public CtField makeField(CtClass proxyCtClass) throws NotFoundException, CannotCompileException {
            return makeField(proxyCtClass, Set.class);
        }

        @SuppressWarnings("unchecked")
        @Override
        public void apply(Object object, Map<Class, Map<String, Object>> classRefObjectMap) {
            try {
                Set<String> referenceKeys = get(object, Set.class, refFieldName);
                if (referenceKeys == null) {
                    return;
                }

                Class<?> fieldType = field.getType();
                Collection collection = fieldType == Set.class ? new HashSet() : new ArrayList();

                Map<String, Object> map = classRefObjectMap.get(proxyMap.get(genericType));
                for (String referenceKey : referenceKeys) {
                    Object referenceValue = map.get(referenceKey);
                    if (referenceValue == null) {
                        throw new ReferenceNotFoundException(genericType, referenceKey);
                    }
                    if (referenceValue instanceof Proxy) {
                        referenceValue = ((Proxy)referenceValue).unwrap();
                    }
                    collection.add(referenceValue);
                }

                set(object, field.getName(), field.getType(), collection);
            } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
                throw new ReferenceValueResolutionException(e);
            }
        }
    }
}
