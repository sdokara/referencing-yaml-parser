package com.sdokara.yaml.parser;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
class ReflectionUtils {
    @SuppressWarnings("unchecked")
    static boolean isBeanField(Class clazz, Field field) {
        int modifiers = field.getModifiers();
        if (!(Modifier.isPrivate(modifiers) || Modifier.isProtected(modifiers)) || Modifier.isStatic(modifiers)) {
            return false;
        }
        Class<?> fieldType = field.getType();
        try {
            Method getter = clazz.getDeclaredMethod(getterName(fieldType, field.getName()));
            if (getter.getReturnType() != fieldType) {
                return false;
            }
            clazz.getDeclaredMethod(setterName(field.getName()), fieldType);
        } catch (NoSuchMethodException e) {
            return false;
        }
        return true;
    }

    static String getterName(Class type, String name) {
        String getterPrefix = type == Boolean.class || type == Boolean.TYPE ? "is" : "get";
        return getterPrefix + capitalizeFirst(name);
    }

    static String setterName(String name) {
        return "set" + capitalizeFirst(name);
    }

    static String capitalizeFirst(String string) {
        return string.substring(0, 1).toUpperCase() + string.substring(1);
    }

    @SuppressWarnings("unchecked")
    static <T> T get(Object object, Class<T> type, String fieldName)
            throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        Method getter = object.getClass().getMethod(getterName(type, fieldName));
        return (T)getter.invoke(object);
    }

    static void set(Object object, String fieldName, Class fieldType, Object value)
            throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        Method setter = object.getClass().getMethod(setterName(fieldName), fieldType);
        setter.invoke(object, value);
    }
}
