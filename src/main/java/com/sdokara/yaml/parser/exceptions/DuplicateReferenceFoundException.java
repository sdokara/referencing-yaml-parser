package com.sdokara.yaml.parser.exceptions;

public class DuplicateReferenceFoundException extends ReferencingYamlParserException {
    public DuplicateReferenceFoundException(Class clazz, String referenceKey) {
        super("Found duplicate [" + clazz.getSimpleName() + ": " + referenceKey + "]");
    }
}
