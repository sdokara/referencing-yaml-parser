package com.sdokara.yaml.parser.exceptions;

public class ProxyingException extends ReferencingYamlParserException {
    public ProxyingException(Class clazz, Throwable cause) {
        super("Unable to proxy " + clazz.getName(), cause);
    }
}
