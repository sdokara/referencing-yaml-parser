package com.sdokara.yaml.parser.exceptions;

public class ReferenceNotFoundException extends ReferencingYamlParserException {
    public ReferenceNotFoundException(Class fieldType, String referenceKey) {
        super("Unable to find [" + fieldType.getSimpleName() + ": " + referenceKey + "]");
    }
}
