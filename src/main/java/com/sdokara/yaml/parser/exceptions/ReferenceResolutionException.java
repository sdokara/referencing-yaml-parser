package com.sdokara.yaml.parser.exceptions;

public class ReferenceResolutionException extends ReferencingYamlParserException {
    public ReferenceResolutionException(Throwable cause) {
        super("Failed enumerating objects by reference", cause);
    }

    public ReferenceResolutionException(String message) {
        super(message);
    }
}
