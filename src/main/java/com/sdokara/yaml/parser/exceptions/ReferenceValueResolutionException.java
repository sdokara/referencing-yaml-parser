package com.sdokara.yaml.parser.exceptions;

public class ReferenceValueResolutionException extends ReferencingYamlParserException {
    public ReferenceValueResolutionException(Throwable cause) {
        super("Unable to apply reference", cause);
    }

    public ReferenceValueResolutionException(String message) {
        super(message);
    }
}
