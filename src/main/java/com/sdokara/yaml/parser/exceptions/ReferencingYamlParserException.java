package com.sdokara.yaml.parser.exceptions;

public class ReferencingYamlParserException extends RuntimeException {
    public ReferencingYamlParserException(String message) {
        super(message);
    }

    public ReferencingYamlParserException(String message, Throwable cause) {
        super(message, cause);
    }
}
