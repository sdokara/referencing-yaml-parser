package com.sdokara.yaml.parser.spring;

import com.sdokara.yaml.parser.ReferencingYamlParser;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.SpringApplicationRunListener;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.env.ConfigurableEnvironment;

public class SpringBootRunListener implements SpringApplicationRunListener {
    private final SpringApplication application;

    @SuppressWarnings("unused")
    public SpringBootRunListener(SpringApplication application, String[] args) {
        this.application = application;
    }

    @Override
    public void starting() {
        ReferencingYamlParser.appendClassLoader(application.getClassLoader());
    }

    @Override
    public void environmentPrepared(ConfigurableEnvironment environment) {
    }

    @Override
    public void contextPrepared(ConfigurableApplicationContext context) {
    }

    @Override
    public void contextLoaded(ConfigurableApplicationContext context) {
    }

    @Override
    public void started(ConfigurableApplicationContext context) {
    }

    @Override
    public void running(ConfigurableApplicationContext context) {
    }

    @Override
    public void failed(ConfigurableApplicationContext context, Throwable exception) {
    }
}
