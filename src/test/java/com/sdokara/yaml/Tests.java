package com.sdokara.yaml;

import com.sdokara.yaml.models.Department;
import com.sdokara.yaml.models.Employee;
import com.sdokara.yaml.models.Named;
import com.sdokara.yaml.models.Skill;
import com.sdokara.yaml.parser.ReferencingYamlParser;
import junit.framework.AssertionFailedError;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.io.InputStream;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import static org.junit.Assert.*;

@RunWith(JUnit4.class)
public class Tests {
    @Test
    public void test() {
        InputStream istream = Tests.class.getResourceAsStream("/data.yml");
        ReferencingYamlParser parser = new ReferencingYamlParser("name");
        parser.registerClasses(Department.class, Employee.class, Skill.class);
        List<Object> objects = parser.parse(istream);

        assertNotNull(objects);
        assertNotEquals(0, objects.size());

        Map<? extends Class<?>, List<Object>> map = objects.stream().collect(Collectors.groupingBy(Object::getClass));
        Set<Department> departments = extract(map, Department.class);
        assertEquals(2, departments.size());
        Set<Employee> employees = extract(map, Employee.class);
        assertEquals(2, employees.size());
        Set<Skill> skills = extract(map, Skill.class);
        assertEquals(3, skills.size());

        Department it = findByName(departments, "IT");
        Department finances = findByName(departments, "Finances");
        Employee john = findByName(employees, "John");
        Employee jane = findByName(employees, "Jane");
        Skill computers = findByName(skills, "Computers");
        Skill speaking = findByName(skills, "Speaking");
        Skill writing = findByName(skills, "Writing");

        assertEquals(1, it.getEmployees().size());
        assertSame(john, it.getEmployees().iterator().next());
        assertSame(it, john.getDepartment());

        assertEquals(1, finances.getEmployees().size());
        assertSame(jane, finances.getEmployees().iterator().next());
        assertSame(finances, jane.getDepartment());

        assertSame(computers, findByName(john.getSkills(), "Computers"));
        assertSame(speaking, findByName(john.getSkills(), "Speaking"));
        assertSame(writing, findByName(john.getSkills(), "Writing"));

        assertSame(computers, findByName(jane.getSkills(), "Computers"));
        assertSame(speaking, findByName(jane.getSkills(), "Speaking"));
    }


    private static <T> Set<T> extract(Map<? extends Class<?>, List<Object>> map, Class<T> clazz) {
        List<Object> objects = map.get(clazz);
        Set<T> result = new LinkedHashSet<>();
        if (objects != null) {
            objects.stream().map(clazz::cast).forEach(result::add);
        }
        return result;
    }

    private static <T extends Named> T findByName(Set<T> set, String name) {
        return set.stream()
                .filter(e -> e.getName().equals(name))
                .findFirst()
                .orElseThrow(() -> new AssertionFailedError("Unable to find object " + name));
    }
}
