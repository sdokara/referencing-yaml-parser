package com.sdokara.yaml.models;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.util.Set;

@Data
public class Department implements Named {
    private String name;
    private String address;
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    private Set<Employee> employees;
}
