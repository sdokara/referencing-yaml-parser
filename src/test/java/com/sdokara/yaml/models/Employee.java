package com.sdokara.yaml.models;

import lombok.Data;

import java.util.Set;

@Data
public class Employee implements Named {
    private String name;
    private Department department;
    private Set<Skill> skills;
}
