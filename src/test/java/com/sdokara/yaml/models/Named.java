package com.sdokara.yaml.models;

public interface Named {
    String getName();
}
