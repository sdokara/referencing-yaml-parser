package com.sdokara.yaml.models;

import lombok.Data;

@Data
public class Skill implements Named {
    private String name;
}
